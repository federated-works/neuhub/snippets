# Hubzilla Utilities

## Add Repo Utility Changes

We made a couple of modifications to the add repo utilities. The major change is that you can now specify the branch for repos. 

This is useful if you are setting up a test environment and want to test the dev branch before releasing the code to the master branch.

The following examples would install the dev branch of the themes, addons, and widgets repositories. 
```
util/add_theme_repo_branch https://framagit.org/federated-works/neuhub/hubzilla-themes.git neuhubthemes dev
util/add_addon_repo_branch https://framagit.org/federated-works/neuhub/hubzilla-addons.git neuhubaddons dev
util/add_widget_repo_branch https://framagit.org/federated-works/neuhub/hubzilla-widgets.git neuhubwidgets dev
```
In the above example, we are installing the `dev` branch of each repository, as indicated by "dev" at the end of the command.

Note: Installing more than one branch of the same repository at the same time would probably be confusing in the Hubzilla interface, since there is no way to tell the difference there.
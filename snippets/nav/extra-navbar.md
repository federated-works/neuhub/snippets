# Extra Navbar Snippet

I have been working on my own websites and some client websites, and one thing that became obvious is that Hubzilla needs more navigation. I found that adding a simple navbar to most pages helps significantly.

Right now I am using the webpages module to create a block, and then using the PDL Editor to place it on various modules, like channel, profile, wiki, etc.

To see an example, check out my channel:  https://codejournal.dev/channel/scott

```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">
  <div class="container-fluid">
    <a class="navbar-brand" href="/channel/example">
      <img src="photo/profile/s/4" alt="" width="30" height="30" />
    </a>
    <a class="navbar-brand" href="/channel/example">Name of Channel</a>
    <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link" href="/channel/example">Posts</a>
        <a class="nav-link" href="/profile/example">Profile</a>
        <a class="nav-link" href="/photos/example">Photos</a>
        <a class="nav-link" href="/cloud/example">Files</a>
        <a class="nav-link" href="/cal/example">Calendar</a>
        <a class="nav-link" href="/wiki/example">Wiki</a>
        <a class="nav-link" href="/articles/example">Articles</a>
      </div>
    </div>
  </div>
</nav>
```

Note: I included all of the common pages in the above example, but for most channels, you will just wants to put Posts and Profile there. Perhaps wiki and articles. These links will typically be used by people who are not logged into your website.

On mobile it is responsive. The other option is to just have this disappear on mobile, because it would be redundant to the drop down menu in the main navbar.

I also want to create a version that can change from light mode to dark mode based on what you pick in Hubzilla. This version is always dark mode.

I eventually want to turn this into a widget, and submit it to the Hubzilla core.  If not accepted there, then it will become part of Neuhub themes.



        
# Snippets

This is just a collection of code that we may want to refer to later. 

Some things that may be contained here:

- HTML that we frequently use for customizing websites.
- Example code that we may want to use in the future.
- Modifications that we might want to add later.

Since these are just random pieces of code, you can't "install" it (like an addon or widget). However, in many cases, the code can be combined with other code to work. For example, the custom Add Repo Branch scripts can be uploaded to your Hubzilla installation and be used in lieu of Hubzilla's versions.

Feel free to browse and see if anything is useful to you, but it may not be documented really well since it is just a place for us to save random snippets of code.

### Credits & Notes

* [Hubzilla](https://hubzilla.org) is maintained by the [Hubzilla Association](https://hubzilla.org).
* [Neuhub](https://neuhub.org) is maintained by [Federated Works](https://federated.works).
* [WisTex TechSero Ltd. Co.](https://wistex.com) is a major contributor of code to this project.

### License

This code is licensed under the [MIT license (Expat Version)](https://framagit.org/federated-works/neuhub/snippets/-/blob/main/LICENSE). 

Copyright &copy; 2021-2024 WisTex TechSero Ltd. Co.
